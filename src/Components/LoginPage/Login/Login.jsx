import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './Login.scss';

const Login = ({
  toggle, dataUser, user, handleChange, getDataUser,
}) => {
  const navigate = useNavigate();
  const submitData = () => {
    const itemData = dataUser.some((item) => item.username === user.username && item.password === user.password);
    console.log({ itemData });
    if (itemData) {
      navigate('/exam-list');
    } else {
      alert('Incorrect information');
    }
  };

  useEffect(() => {
    getDataUser();
  }, [user]);

  return (
    <div className="box-form">
      <div className="form">
        <div className="border-box">
          <h3 className="title">LOGIN</h3>
          <div className="inputArea">
            <label htmlFor="">Username</label>
            <input type="text" name="username" value={user.username} className="userName" onChange={handleChange} />
          </div>
          <div className="inputArea">
            <label htmlFor="">Password</label>
            <input type="password" name="password" value={user.password} className="passWord" onChange={handleChange} />
          </div>
          <div className="group-button">
            <div className="itemBtn">
              <button className="btn" onClick={submitData}>Submit</button>
            </div>
            <div className="itemBtn">
              <button className="btn" onClick={toggle}>Register</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Login;
