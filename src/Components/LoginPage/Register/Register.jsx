import React, { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../Login/Login.scss';
import { createUser } from '../../../Fetcher/user.fetcher';

const Register = ({
  toggle, user, dataUser, handleChange, reset, getDataUser,
}) => {
  const successPopup = () => {
    toast.success('Successful', {
      position: 'top-right',
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };
  const submitData = () => {
    console.log({ user });
    const checkDup = dataUser.some((item) => item.username === user.username);
    console.log({ checkDup });
    if (!checkDup) {
      createUser(user).then((data) => {
        // console.log({ data });
        successPopup();
        reset();
        getDataUser();
      });
    } else {
      reset();
      alert('select another username');
    }
  };
  useEffect(() => {
    getDataUser();
  }, []);

  return (
    <div className="box-form">
      <div className="form">
        <div className="border-box">
          <h3 className="title">REGISTER</h3>
          <div className="inputArea">
            <label htmlFor="">Username</label>
            <input type="text" name="username" value={user.username} className="userName" onChange={handleChange} />
          </div>
          <div className="inputArea">
            <label htmlFor="">Password</label>
            <input type="password" name="password" value={user.password} className="passWord" onChange={handleChange} />
          </div>
          <div className="group-button">
            <div className="itemBtn">
              <button className="btn" onClick={submitData}>Register</button>
              <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
            </div>
            <div className="itemBtn">
              <button className="btn" onClick={toggle}>Login</button>
            </div>
          </div>

        </div>
      </div>
    </div>
  );
};
export default Register;
