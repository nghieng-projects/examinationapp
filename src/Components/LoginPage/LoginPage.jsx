import React, { useEffect, useState } from 'react';
import './LoginPage.scss';
import Login from './Login';
import Register from './Register';
import { getDataRegister } from '../../Fetcher/user.fetcher';

function LoginPage() {
  const [isShowRegister, setIsShowRegister] = useState(false);
  const toggleShowRegister = () => {
    setIsShowRegister(!isShowRegister);
  };

  const [user, setUser] = useState({
    username: '',
    password: '',
  });

  const handleChange = ({ target }) => {
    const { name, value } = target;
    setUser((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const resetForm = () => {
    setUser({ username: '', password: '' });
  };

  const [dataUser, setDataUser] = useState([]);
  const getDataUser = () => {
    getDataRegister().then((data) => {
      console.log({ data });
      setDataUser(data);
    });
  };
  useEffect(() => {
    getDataUser();
  }, []);
  return (
    <div className="LoginPage">

      {
                isShowRegister === true
                  ? (
                    <Register
                      toggle={toggleShowRegister}
                      user={user}
                      dataUser={dataUser}
                      handleChange={handleChange}
                      getDataUser={getDataUser}
                      reset={resetForm}
                    />
                  )
                  : (
                    <Login
                      toggle={toggleShowRegister}
                      dataUser={dataUser}
                      user={user}
                      getDataUser={getDataUser}
                      handleChange={handleChange}
                    />
                  )
            }

    </div>
  );
}

export default LoginPage;
