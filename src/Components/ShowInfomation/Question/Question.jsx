import React from 'react';
import PropTypes from 'prop-types';
import './Question.scss';

const Question = ({ question, id, onChange }) => (
  <div className="Question">
    <div className="border-box">
      <div className="border-inside">
        <p>
          Question:
          {' '}
          {id}
        </p>
        <h4 className="question">{question.name}</h4>
        <div className="answers">
          {
                question.answers.map((item) => (
                  <div className="group-answer" key={item.id}>
                    <input type="radio" name={id} value={item.value} onChange={() => { onChange(item, id); }} />
                    <span>
                      {item.value}
                    </span>
                  </div>
                ))
              }
        </div>
      </div>
    </div>
  </div>
);
Question.propsTypes = {
  question: PropTypes.shape.isRequired,
  id: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};
export default Question;
