import React, {useState, useEffect} from "react";
import "./ShowInfomation.scss"
import Question from "./Question";
import {useLocation} from "react-router-dom";
import Modal from 'react-modal'
import Scorepopup from "../Scorepopup";
import CountdownTimer from "../CountdownTimer";
import {getExamById} from "../../Fetcher/exam.fetcher";
import {postUserAnswer} from "../../Fetcher/user.fetcher";

const useQuery = () => {
    const {search} = useLocation();
    return React.useMemo(() => new URLSearchParams(search), [search]);
}

const ShowInfomation = () => {
    const query = useQuery()
    const id = query.get('id');
    const [disableBtn, setDisableBtn] = useState(false)
    const [totalQuestions, setTotalQuestion] = useState(null)
    const [trueAnswer, setTrueAnswer] = useState(null)
    const [falseAnswer, setFalseAnswer] = useState(null)
    const [questionInformation, setQuestionInformation] = useState(null)
    const getQuestionData = () => {
        getExamById(id).then((data) => {
            setQuestionInformation(data)
            console.log({data})
            setTotalQuestion(data.questions.length)
        })
    }


    const [userAnswers, setUserAnswers] = useState([]);
    const [adminAnswers, setAdminAnswers] = useState([])
    const handleUserAnswer = (item, idQuestion) => {
        console.log({item, idQuestion})
        setUserAnswers((prevState) => {
            const newAnswer = {
                idQuestion: idQuestion,
                indexAnswer: item.id,
                valueCorrect: item.is_correct
            };
            console.log({newAnswer})
            const resultDupFilter = prevState.filter((it) => {
                return it.idQuestion !== newAnswer.idQuestion
            })
            resultDupFilter.push(newAnswer)
            // sort base on grow up number, when user check random => make idnumber random
            return resultDupFilter.sort((prev, next) => {
                return prev.idQuestion - next.idQuestion
            });

        });


        // setAdminAnswers((prevState) => {
        //     const trueAnswer = questionInformation.questions[idQuestion].answers.filter((el) => {
        //         return el.is_correct;
        //     })
        //     console.log({trueAnswer})
        //
        //     const newAdminAnswer = {
        //         idQuestion: idQuestion,
        //         indexAnswer: trueAnswer[0].id,
        //         valueCorrect: trueAnswer[0].is_correct
        //     }
        //     const Arr = []
        //     Arr.push(newAdminAnswer)
        //     return [...prevState,...Arr].sort((prev, next) => {
        //         return prev.idQuestion - next.idQuestion
        //     })
        // })
    };


    // const functionCompare = (userAnswerArr, adminAnswerArr) => {
    //     return userAnswerArr.idQuestion === adminAnswerArr.idQuestion && userAnswerArr.valueCorrect === adminAnswerArr.valueCorrect
    // }
    //
    // const checkFalse = (userAnswerArr, adminAnswerArr) => {
    //     return userAnswerArr.filter((userAnswerItem) => !adminAnswerArr.some((adminAnswerItem) => {
    //         return functionCompare(userAnswerItem, adminAnswerItem)
    //     }))
    // }
    // const checkTrue = (userAnswerArr, adminAnswerArr) => {
    //     return userAnswerArr.filter((userAnswerItem) => adminAnswerArr.some((adminAnswerItem) => {
    //         return functionCompare(userAnswerItem, adminAnswerItem)
    //     }))
    // }

    const [openModal, setOpenModal] = useState(false)
    const isOpenModal = () => {
        setOpenModal(true)
    }
    const isCloseModal = () => {
        setOpenModal(false)
    }
    /*set css for modal*/
    const modalStyles = {
        content: {
            width:'30%',
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
        },
    };


    const [userInfo, setUserInfo] = useState({
        username: '',
        email: '',
        answers:[]
    });
    const getInformation = ({target}) => {
        const {name} = target;
        const {value} = target;
        setUserInfo((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };
    const [idresult,setIdResult] = useState(null)
    const getResult = () => {
        if(userAnswers.length === questionInformation.questions.length){
            console.log({userAnswers})
            const filterTrue = userAnswers.filter((answer)=>{ return answer.valueCorrect === true})
            const filterFalse = userAnswers.filter((answer)=>{return answer.valueCorrect === false})
            setTrueAnswer(filterTrue)
            setFalseAnswer(filterFalse)
            const payload = {...userInfo,answers :userAnswers,}
            console.log({filterTrue,filterFalse})
            postUserAnswer(payload).then((data)=>{
                console.log(data)
                setIdResult(data.id)
            })
            isOpenModal()
        }else{
            alert('NOT ENOUGH ANSWERS')
        }
    }
    useEffect(()=>{

    },[])
    useEffect(() => {
        getQuestionData();
    }, []);


    return (
        <div className="showInformation">
            <div className="userInformation">
                <label htmlFor="">
                    <span>User's name:</span>
                    <input type="text" name="username" autoComplete="off"
                           placeholder="Enter your name"
                           onChange={getInformation}/>
                </label>
                <label>
                    <span>Email:</span>
                    <input type="text" name="email" autoComplete="off"
                           placeholder="Enter your email"
                           onChange={getInformation}/>
                </label>
            </div>
            {
                questionInformation && (
                    <div>
                        <h3 className="headTitle">{questionInformation.name}</h3>
                        <h3 className="numberQuestion">1 -> {questionInformation.questions.length}</h3>
                        <h3 className="countDownTime"><CountdownTimer time={questionInformation.time} setDisableBtn={setDisableBtn}/></h3>
                        {
                            questionInformation.questions.map((item, index) => {
                                return (
                                    <Question questionInformation={questionInformation} question={item} questionId={item.id}
                                              answer={item.answers} id={index + 1} key={index}
                                              onChange={(item,idQuestion) => handleUserAnswer(item, idQuestion)}/>
                                )
                            })
                        }
                        <button className="btn" onClick={getResult} disabled={disableBtn}>Finish</button>

                        <Modal
                            isOpen={openModal}
                            onRequestClose={isCloseModal}
                            contentLabel="Example Modal"
                           /* style={modalStyles}*/
                            className="Modal"
                            overlayClassName="Overlay"
                        >
                            <Scorepopup totalNumber={totalQuestions} idExam={id} trueNumber={trueAnswer} falseNumber={falseAnswer}
                            userAnswers={userAnswers} idresult={idresult}/>
                        </Modal>
                    </div>
                )
            }
        </div>
    )
}
export default ShowInfomation