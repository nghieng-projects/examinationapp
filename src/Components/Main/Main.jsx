import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

import './Main.scss';
import LoginPage from '../LoginPage';
import Scorepopup from '../Scorepopup';
import ShowInfomation from '../ShowInfomation';
import ExaminationList from '../ExaminationList';
import AddNewQuestion from '../AddNewQuestion';
import Dashboard from '../Dashboard';
import CandidateExamList from '../CandidateExamList';
import PopupDescription from '../PopupDescriptiom';
import DetailResult from '../DetailResult';
import CountdownTimer from '../CountdownTimer';
import CountTimer from '../CountTimer';
import { getAllExamList } from '../../Fetcher/exam.fetcher';

function Main() {
  const [examList, setExamList] = useState([]);

  const getExamListdata = () => {
    getAllExamList().then((data) => {
      setExamList(data);
    });
  };

  useEffect(() => {
    getExamListdata();
  }, []);
  return (
    <div className="Main">
      <Router>
        <Routes>
          <Route path="/count" element={<CountTimer />} />
          <Route path="/count-timer" element={<CountdownTimer />} />
          <Route path="/detail-result" element={<DetailResult />} />
          <Route path="/popup-description" element={<PopupDescription />} />
          <Route path="/candidate-exam-list" element={<CandidateExamList examList={examList} />} />
          <Route
            path="/exam-list"
            element={<ExaminationList examList={examList} getExamList={getExamListdata} />}
          />
          <Route path="/score-popup" element={<Scorepopup />} />
          <Route
            path="/show-infomation"
            element={(
              <ShowInfomation />
            )}
          />
          <Route
            path="/addnew-question"
            element={<AddNewQuestion getExamListData={getExamListdata} />}
          />
          <Route path="/login-page" element={<LoginPage />} />
          <Route path="/" exact element={<Dashboard />} />
        </Routes>
      </Router>
    </div>
  );
}

export default Main;
