import React, { useEffect, useState } from 'react';

const nameList = ['Dam Dam', 'Nghieng Nghieng', 'Henry', 'Pumkin'];
const messageList = ['Hello', 'Xin chào', 'Hallo', 'สวัสดี'];

const MyName = () => {
  const [name, setName] = useState(nameList[0]);
  const [message, setMessage] = useState(messageList[0]);

  const onChangeName = () => {
    const randName = nameList[Math.floor(Math.random() * nameList.length)];
    setName(randName);
    console.log({ name });
  };

  useEffect(() => {
    const randMessage = messageList[Math.floor(Math.random() * messageList.length)];

    setMessage(`${randMessage}, ${name}`);
  }, [name]);

  return (
    <div>
      <div>{message}</div>
      <div>
        <button onClick={onChangeName}>Random</button>
      </div>
    </div>
  );
};
export default MyName;
