import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import './Scorepopup.scss';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import DetailResult from '../DetailResult';

const localCartKey = 'UserResult';

const Scorepopup = ({
  totalNumber, trueNumber, falseNumber, idExam, userAnswers, idresult,
}) => {
  console.log({
    idExam, trueNumber, falseNumber, userAnswers, idresult,
  });

  const addLocalStorage = () => {
    const stringUserAnswer = JSON.stringify(userAnswers);
    localStorage.setItem(localCartKey, stringUserAnswer);
  };

  const [openModal, setOpenModal] = useState(false);
  // const isOpenModal = () => {
  //   setOpenModal(true);
  // };
  const isCloseModal = () => {
    setOpenModal(false);
  };

  useEffect(() => {
    addLocalStorage();
  }, []);

  return (
    <div className="popup">
      <div className="border-box">
        <h3 className="title">Score:</h3>
        <h1 className="score">{Math.floor((trueNumber.length * 100) / totalNumber)}</h1>
        <h4 className="detailNumber">
          Correct Answer :
          {' '}
          {trueNumber.length}
          {' '}
          /
          {' '}
          {totalNumber}
        </h4>
        <div>
          <FontAwesomeIcon icon={faCheckCircle} style={{ color: '#00CC33' }} />
          {trueNumber.map((item) => (
            <span style={{ fontWeight: '500' }}>
              {' '}
              {item.idQuestion}
              {' '}
            </span>
          ))}
        </div>
        <div>
          <FontAwesomeIcon icon={faTimesCircle} style={{ color: '#FF3333' }} />
          {falseNumber.map((item) => (
            <span style={{ fontWeight: '500' }}>
              {' '}
              {item.idQuestion}
              {' '}
            </span>
          ))}
        </div>
        <div className="group-button">
          <NavLink to={`/detail-result/?id=${idExam}&idresult=${idresult}`}>
            <button className="btn">Detail Result</button>
          </NavLink>
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a><button className="btn" onClick={() => window.location.reload()}>Back Prev Exam</button></a>
          <NavLink to="/candidate-exam-list">
            <button className="btn">Ok</button>
          </NavLink>
        </div>
      </div>
      <Modal
        isOpen={openModal}
        onRequestClose={isCloseModal}
      >
        <DetailResult />
      </Modal>
    </div>

  );
};
Scorepopup.propsTypes = {
  totalNumber: PropTypes.number.isRequired,
  trueNumber: PropTypes.array.isRequired,
  falseNumber: PropTypes.array.isRequired,
  score: PropTypes.number.isRequired,
};
export default Scorepopup;
