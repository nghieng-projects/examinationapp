import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { Animated } from 'react-animated-css';
import './Dashboard.scss';
import Modal from 'react-modal';
import Guideline from './Guideline';

const Dashboard = () => {
  const [openModal, setOpenModal] = useState(false);
  const isOpenModal = () => {
    setOpenModal(true);
  };
  const isCloseModal = () => {
    setOpenModal(false);
  };
  return (
    <div className="Dashboard">
      <div className="item">
        <NavLink to="/login-page" className="dashboardBtn">Admin</NavLink>
        <button onClick={isOpenModal} className="dashboardBtn">Guideline</button>
        <Modal
          isOpen={openModal}
          onRequestClose={isCloseModal}
          closeTimeoutMS={200}
          className="ModalGuideLine"
          overlayClassName="OverlayGuideLine"
        >
          <Guideline closeModal={isCloseModal} />
        </Modal>

        {/* <NavLink to="/count">Start Countdown</NavLink> */}
      </div>
      <div className="item">
        <Animated
          animationIn="fadeInDown"
          animationOut="zoomOutDown"
          animationInDuration={1000}
          animationOutDuration={1000}
          isVisible
        >
          <h3 className="dashboard-title">Have you prepared for the exam yet?</h3>
          <h3 className="dashboard-title">Press Start, choose the exam and finish it!</h3>
        </Animated>
      </div>
      <div className="item">
        <NavLink to="/candidate-exam-list" className="start-button">Start</NavLink>
      </div>
    </div>
  );
};

export default Dashboard;
