import React from 'react';
import './Guideline.scss';

const Guideline = ({ closeModal }) => (
  <div className="guideline">
    <div className="contentGuideLine">
      <p>This is a project to make examinations.</p>
      <p>Have 2 UIs, for admin to create exams and for candidates to finish exams.</p>
      <p>
        Press Admin button, let register to create a new acc and login, after finish login progressive, feel
        free to create the test types you need.
      </p>
      <p>Candidates press Start button to select the exam that they want to complete.</p>
      {/* eslint-disable-next-line react/no-unescaped-entities */}
      <p style={{ textAlign: 'center' }}>Without further delay, let's begin.</p>
    </div>
    <button onClick={closeModal} className="closeModal">Close</button>
  </div>
);
export default Guideline;
