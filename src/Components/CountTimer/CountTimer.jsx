import React, { useState, useEffect } from 'react';

const CountTimer = () => {
  const [time, setTime] = useState(0);
  const [timeOn, setTimeOn] = useState(false);

  useEffect(() => {
    let interval = null;
    if (timeOn) {
      interval = setInterval(() => {
        setTime((prevState) => prevState + 1);
      }, 10);
    } else {
      clearInterval(interval);
    }

    return () => {
      clearInterval(interval);
    };
  }, [timeOn]);

  return (
    <div>
      <h3>{time}</h3>
      <button onClick={() => setTimeOn(true)}>Start</button>
      <button onClick={() => setTimeOn(false)}>Stop</button>
      <button onClick={() => setTime(0)}>Reset</button>
    </div>
  );
};
export default CountTimer;
