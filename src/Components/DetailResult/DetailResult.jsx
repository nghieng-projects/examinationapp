import React, { useEffect, useState } from 'react';
import '../ShowInfomation/ShowInfomation.scss';
import { NavLink, useLocation } from 'react-router-dom';
import QuestionItem from './QuestionItem';
import { getExamById } from '../../Fetcher/exam.fetcher';
import { getUserAnswer } from '../../Fetcher/user.fetcher';

const useQuery = () => {
  const { search } = useLocation();
  return React.useMemo(() => new URLSearchParams(search), [search]);
};

const DetailResult = () => {
  const query = useQuery();
  const id = query.get('id');
  const idResult = query.get('idresult');
  console.log({ idResult });
  const [dataExam, setDataExam] = useState(null);
  const [userData, setUserData] = useState(null);
  const getData = () => {
    getExamById(id).then((data) => {
      setDataExam(data);
      console.log({ data });
    });
    getUserAnswer(idResult).then((dataUser) => {
      console.log({ dataUser });
      setUserData(dataUser);
    });
  };

  useEffect(() => {
    getData();
  }, []);

  if (!userData) {
    return null;
  }

  return (
    <div className="showInformation">
      <div className="userInformation">
        <label htmlFor="">
          <span>
            User&#39;s name:
          </span>
          <h3>{userData.username}</h3>
        </label>
        <label>
          <span>Email:</span>
          <h3>{userData.email}</h3>
        </label>
      </div>
      {
                dataExam && (
                <div>
                  <h3 className="headTitle">{dataExam.name}</h3>
                  <h4 className="numberQuestion">
                    1 -
                    {'>'}
                    {dataExam.questions.length}
                  </h4>
                  <h2 className="countDownTime">
                    Time:
                    {dataExam.time}
                    {' '}
                    minutes
                  </h2>
                  {
                            dataExam.questions.map((item, index) => {
                              const userAnswer = userData.answers.find((userItem) => userItem.idQuestion === item.idQuestion);
                              console.log({ userAnswer });
                              return (
                                <QuestionItem
                                  question={item}
                                  userAnswer={userAnswer}
                                  answer={item.answers}
                                  id={index + 1}
                                  key={item}
                                />
                              );
                            })
                        }

                </div>
                )
            }
      <NavLink to="/candidate-exam-list">
        <button className="btn">Back</button>
      </NavLink>
    </div>
  );
};
export default DetailResult;
