import React from 'react';
import '../../ShowInfomation/Question/Question.scss';
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const QuestionItem = ({ question, userAnswer, id }) => {
  let correctAnswer;

  return (
    <div className="Question">
      <div className="border-box">
        <div className="border-inside">
          <p>
            Question:
            {' '}
            {id}
          </p>
          <h4 className="question">{question.name}</h4>
          <div className="answers">
            {
                            question.answers.map((item) => {
                              if (item.is_correct) {
                                correctAnswer = item;
                              }
                              return (
                                <div className="group-answer" key={item.id}>
                                  <div className="tickIcon">
                                    {userAnswer && userAnswer.indexAnswer === item.id && item.is_correct ? (
                                      <FontAwesomeIcon icon={faCheckCircle} style={{ color: '#00CC33' }} />
                                    ) : ''}
                                    {userAnswer && userAnswer.indexAnswer === item.id && !item.is_correct ? (
                                      <FontAwesomeIcon icon={faTimesCircle} style={{ color: 'red' }} />
                                    ) : ''}
                                  </div>

                                  <input
                                    type="radio"
                                    disabled="disabled"
                                    checked={userAnswer && userAnswer.idAnswer === item.id ? 'checked' : ''}
                                  />
                                  <span>
                                    {item.value}
                                  </span>
                                  {' '}

                                </div>
                              );
                            })
                        }

          </div>
          <div>
            <h3>
              Answer:
              {' '}
              {correctAnswer.value}
            </h3>
          </div>
        </div>
      </div>
    </div>
  );
};
export default QuestionItem;
