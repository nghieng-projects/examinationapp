import React, { useEffect, useState } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import './AddNewQuestion.scss';
import { getExamById, addExamQuestion, updateExamQuestion } from '../../Fetcher/exam.fetcher';

const useQuery = () => {
  const { search } = useLocation();
  return React.useMemo(() => new URLSearchParams(search), [search]);
};

function AddNewQuestion({ getExamListData }) {
  const query = useQuery();
  const id = query.get('id');

  const [examination, setExamination] = useState({
    name: '',
    descriptions: '',
    time: null,
  });
  const [idQues, setIdQues] = useState(1);
  const newRow = {
    idQuestion: idQues,
    name: '',
    answers: [
      {
        id: 1,
        value: '',
        is_correct: false,
      },
      {
        id: 2,
        value: '',
        is_correct: false,
      },
      {
        id: 3,
        value: '',
        is_correct: false,
      },
      {
        id: 4,
        value: '',
        is_correct: false,
      },
    ],
  };
  const [questions, setQuestions] = useState([newRow]);
  const insertRow = () => {
    // eslint-disable-next-line no-plusplus
    setIdQues(idQues + 1);
    newRow.idQuestion = idQues + 1;
    console.log(newRow.idQuestion);
    questions.push(newRow);
    setQuestions([...questions]);
  };

  const handleInfoExam = ({ target }) => {
    const { value, name } = target;
    // console.log({ value, name });
    setExamination((prevState) => ({ ...prevState, [name]: value }));
  };

  // setState new question from input
  const handleQuestion = (index, { target }) => {
    const getValueQuestion = target.value;
    // console.log({ getValueQuestion, index });
    setQuestions((prevState) => {
      const item = prevState[index];
      console.log({ item });
      item.name = getValueQuestion;
      return [...prevState];
    });
  };
    // handle 4 input options

  const handleOption = (questionIndex, answerIndex, { target }) => {
    const { value } = target;
    setQuestions((prevState) => {
      const question = prevState[questionIndex];
      const answer = question.answers[answerIndex];
      answer.value = value;
      prevState[questionIndex] = question;
      return [...prevState];
    });
  };

  const valueSelectAnswer = (index, { target }) => {
    const { value } = target;
    console.log({ value });
    console.log({ index });

    setQuestions((prevState) => {
      const question = prevState[index];
      console.log({ question });
      // return default false
      question.answers = question.answers.map((item) => {
        item.is_correct = false;
        return item;
      });
      // change false -> true
      question.answers[value].is_correct = true;
      // update prevState in current index = value have been update false -> true
      prevState[index] = question;
      // return array have all item
      return [...prevState];
    });
  };

  const getPrevData = () => {
    if (id !== null) {
      getExamById(id)
      // fetch(`http://localhost:3000/data/${id}`)
        .then((data) => {
          const newExamination = {
            name: data.name,
            descriptions: data.descriptions,
            time: data.time,
          };
          setExamination(newExamination);
          setQuestions(data.questions);
        });
    }
  };

  const createExam = () => {
    if (examination.name !== '' && examination.descriptions !== '' && examination.time !== null) {
      const payLoad = { ...examination, questions };
      console.log({ payLoad });
      addExamQuestion(payLoad).then((data) => {
        console.log({ data });
        getExamListData();
      });
    } else {
      alert('not enough information');
    }
  };
  const updateExam = () => {
    const payLoad = { ...examination, questions };
    updateExamQuestion(payLoad, id).then((data) => {
      console.log({ data });
      getExamListData();
    });
  };

  const trashIcon = (item) => {
    console.log({ item });
    const filterQuestion = questions.filter((question) => question.idQuestion !== item.idQuestion);
    console.log(filterQuestion);
    setQuestions([...filterQuestion]);
  };

  const titleInput = ['Answer A', 'Answer B', 'Answer C', 'Answer D'];
  useEffect(() => {
    getPrevData();
  }, []);

  return (
    <div className="elementPage">
      <div className="AddQuestion">
        <h3 className="title">DETAIL QUESTION</h3>
        <div className="groupBasicInformation">
          <div className="basicInformation">
            <h4>Name</h4>
            <input
              type="text"
              autoComplete="off"
              name="name"
              value={examination.name}
              onChange={handleInfoExam}
            />
          </div>
          <div className="basicInformation">
            <h4>Description</h4>
            <textarea
              name="descriptions"
              value={examination.descriptions}
              onChange={handleInfoExam}
              autoComplete="off"
            />
          </div>
          <div className="basicInformation">
            <h4>Timer</h4>
            <input
              type="number"
              name="time"
              placeholder="minutes"
              value={examination.time}
              onChange={handleInfoExam}
              autoComplete="off"
            />
          </div>
        </div>
        <div className="Questions">
          <div className="table-1">
            <div className="row">
              <div className="cell">
                <p>Id Exam</p>
              </div>
              <div className="cell">
                <p>Question</p>
              </div>
              {
                  titleInput.map((title) => (
                    <div className="cell">
                      <p>{title}</p>
                    </div>
                  ))
              }
              <div className="cell">
                <p>Result</p>
              </div>
              <div className="cell">
                <p>Delete</p>
              </div>
            </div>
            <div className="rowQuestion">

              {
                questions.map((row, index) => {
                  const correctAnswer = row.answers.findIndex((item) => item.is_correct);
                  return (
                    <div className="item" key={row}>
                      <div className="cell">
                        <input type="number" className="idShow" value={index + 1} readOnly />
                      </div>
                      <div className="cell">
                        <input
                          type="text"
                          value={row.name}
                          name="name"
                          onChange={(event) => handleQuestion(index, event)}
                        />
                      </div>
                      {
                                row.answers.map((answer, answerIndex) => (
                                  <div className="cell">
                                    <input
                                      type="text"
                                      value={answer.value}
                                      onChange={(event) => {
                                        handleOption(index, answerIndex, event);
                                      }}
                                    />
                                  </div>
                                ))
                            }
                      <div className="cell">
                        <select
                          style={{ color: '#000000' }}
                          name=""
                          id=""
                          onChange={(event) => valueSelectAnswer(index, event)}
                        >
                          <option value="">Select answer</option>
                          <option value="0" selected={correctAnswer === 0}>A</option>
                          <option value="1" selected={correctAnswer === 1}>B</option>
                          <option value="2" selected={correctAnswer === 2}>C</option>
                          <option value="3" selected={correctAnswer === 3}>D</option>
                        </select>
                      </div>
                      <div className="cell">
                        <FontAwesomeIcon className="icon" icon={faTrash} onClick={() => { trashIcon(row); }} />
                      </div>
                    </div>
                  );
                })
            }
            </div>
          </div>
        </div>
        <div className="group-btn">
          <FontAwesomeIcon className="icon-insert" icon={faPlusSquare} onClick={insertRow} />
          {/* eslint-disable-next-line react/button-has-type */}
          <NavLink to="/exam-list">
            {/* eslint-disable-next-line react/button-has-type */}
            {id ? <button className="finish-btn" onClick={updateExam} style={{ color: '#000000' }}>Update</button>
              : <button className="finish-btn" onClick={createExam} style={{ color: '#000000' }}>Create</button>}
          </NavLink>
        </div>
      </div>
    </div>
  );
}

AddNewQuestion.prototype = {
  getExamListData: PropTypes.func.isRequired,
};
export default AddNewQuestion;
