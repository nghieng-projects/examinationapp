import React, { useEffect, useState } from 'react';
import './CountdownTimer.scss';
import PropTypes from 'prop-types';

const CountdownTimer = ({ time, setDisableBtn }) => {
  const [timer, setTimer] = useState({
    hours: 0,
    minutes: 0,
    seconds: 0,
  });
  const countDown = (timeToCount) => {
    // Get today's date and time
    const now = new Date().getTime();

    // Find the distance between now and the count down date
    const distance = timeToCount - now;

    // Time calculations for days, hours, minutes and seconds
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    return {
      hours,
      minutes,
      seconds,
    };
  };

  const examMinutes = time;
  const currentTime = new Date().getTime(); // In milliseconds
  const examTime = examMinutes * 60 * 1000; // Convert from minute to milliseconds
  const timeToCount = currentTime + examTime + 1000;

  useEffect(() => {
    const interval = setInterval(() => {
      const countData = countDown(timeToCount);
      setTimer(countData);

      if (countData.hours === 0 && countData.minutes === 0 && countData.seconds === 0) {
        clearInterval(interval);
        setDisableBtn(true);
      }
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <h3>
      {/* {`${timer.hours}0:${timer.minutes}:${timer.seconds}`} */}
      {
        timer.hours < 10 ? `0${timer.hours}` : `${timer.hours}`
      }
      :
      {
        timer.minutes < 10 ? `0${timer.minutes}` : `${timer.minutes}`
      }
      :
      {
        timer.seconds < 10 ? `0${timer.seconds}` : `${timer.seconds}`
      }
    </h3>
  );
};
CountdownTimer.prototype = {
  time: PropTypes.number.isRequired,
};
export default CountdownTimer;
