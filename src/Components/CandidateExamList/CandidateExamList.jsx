import React from 'react';
import { Animated } from 'react-animated-css';
import './CandidateExamList.scss';
import { NavLink } from 'react-router-dom';

function CandidateExamList({ examList }) {
  return (
    <div className="elementPage">
      <div className="candidateExamList">
        <h3 className="title">EXAMINATION LIST</h3>
        <div className="group-exam">
          {
                examList.map((item) => (
                  /* {`/show-infomation/?id=${item.id}`} */
                  <NavLink to={`/popup-description/?id=${item.id}`} className="exam">
                    <Animated
                      animationIn="bounceInLeft"
                      animationOut="zoomOutDown"
                      animationInDuration={1000}
                      animationOutDuration={1000}
                      isVisible
                    >
                      <h4 className="name-exam">{item.name}</h4>
                    </Animated>
                  </NavLink>
                ))
            }
        </div>
      </div>
    </div>
  );
}

export default CandidateExamList;
