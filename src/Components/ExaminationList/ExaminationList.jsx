import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import './ExaminationList.scss';
import List from './List';

function ExaminationList({ examList, getExamList }) {
  return (
    <div className="elementPage">
      <div className="examinationList">
        <h3 className="title">EXAMINATION LIST</h3>
        <List examList={examList} getExamList={getExamList} />
        <div className="button">
          <NavLink to="/">
            <button type="submit" className="btn">Home</button>
          </NavLink>
          <NavLink to="/addnew-question">
            <button type="submit" className="btn">Create</button>
          </NavLink>
        </div>
      </div>
    </div>
  );
}
ExaminationList.propTypes = {
  examList: PropTypes.arrayOf.isRequired,
  getExamList: PropTypes.func.isRequired,
};
export default ExaminationList;
