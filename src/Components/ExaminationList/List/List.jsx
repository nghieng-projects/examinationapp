import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from 'react-router-dom';
import './List.scss';
import { deleteExamById } from '../../../Fetcher/exam.fetcher';

function List({ examList, getExamList }) {
  const handleDelete = (id) => {
    const confirmDelete = window.confirm('Do you want to delete this examination?');
    if (confirmDelete) {
      deleteExamById(id).then((data) => {
        getExamList();
      });
    }
  };

  return (
    <div className="List">
      {
                examList.map((item) => (
                  <div className="group-list" key={item}>
                    <NavLink to="/exam-list"><h2 className="listExam">{item.name}</h2></NavLink>
                    {/* {`/show-infomation/?id=${item.id}`} */}
                    <div className="icons">
                      <NavLink to={`/addnew-question/?id=${item.id}`}><FontAwesomeIcon className="editIcon" icon={faEdit} /></NavLink>
                      <FontAwesomeIcon className="trashIcon" onClick={() => handleDelete(item.id)} icon={faTrash} />
                    </div>
                  </div>
                ))
            }

    </div>
  );
}
List.propTypes = {
  examList: PropTypes.shape.isRequired,
  getExamList: PropTypes.func.isRequired,
};
export default List;
