import React, { useEffect, useState } from 'react';
import './PopupDescription.scss';
import { NavLink, useLocation } from 'react-router-dom';
import { getExamById } from '../../Fetcher/exam.fetcher';

const useQuery = () => {
  const { search } = useLocation();
  return React.useMemo(() => new URLSearchParams(search), [search]);
};

function PopupDescription() {
  const query = useQuery();
  const id = query.get('id');

  const [examList, setExamList] = useState(null);
  const getExamList = () => {
    getExamById(id).then((data) => {
      setExamList(data);
      // console.log({ data });
    });
  };

  useEffect(() => {
    getExamList();
  }, []);
  return (
    <div className="elementPage">
      <div className="PopupDescription">
        <h3 className="title">Examination Information</h3>

        {
          examList && (
          <div className="detail-information">
            <h3 style={{ fontWeight: '400' }}>
              Name:
              {' '}
              {examList.name}
            </h3>
            <p>
              Description:
              {' '}
              {examList.descriptions}
            </p>
            <p>
              Time:
              {' '}
              {examList.time}
              {' '}
              minutes
            </p>
            <NavLink to={`/show-infomation/?id=${examList.id}`}><button>Let&#39;s start</button></NavLink>
          </div>
          )
        }

      </div>
    </div>
  );
}
export default PopupDescription;
