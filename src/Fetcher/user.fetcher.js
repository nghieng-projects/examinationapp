const apiHost = process.env.REACT_APP_API_HOST;

export const createUser = (user) => fetch(`${apiHost}/register`, {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify(user),
}).then((response) => response.json());

export const getDataRegister = () => fetch(`${apiHost}/register`)
  .then((response) => response.json());

export const postUserAnswer = (userAllInfo) => fetch(`${apiHost}/useranswer`, {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify(userAllInfo),
}).then((response) => response.json());

export const getUserAnswer = (id) => fetch(`${apiHost}/useranswer/${id}`)
  .then((response) => response.json());
