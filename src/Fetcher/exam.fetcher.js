const apiHost = process.env.REACT_APP_API_HOST;

export const getAllExamList = () => fetch(`${apiHost}/data`)
  .then((response) => response.json());

export const getExamById = (id) => fetch(`${apiHost}/data/${id}`)
  .then((response) => response.json());

export const addExamQuestion = (payLoad) => fetch(`${apiHost}/data`, {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify(payLoad),
}).then((response) => response.json());

export const updateExamQuestion = (payLoad, id) => fetch(`${apiHost}/data/${id}`, {
  method: 'PUT',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify(payLoad),
}).then((response) => response.json());

export const deleteExamById = (id) => fetch(`${apiHost}/data/${id}`, {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(),
}).then((response) => response.json());
