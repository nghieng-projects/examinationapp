module.exports = {
  env: {
    browser: true,
  },
  extends: [
    'airbnb',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'no-console': 'off',
    'no-param-reassign': 'off',
    'no-array-index-key': 'off',
    'react/prop-types': 'off',
    'react/function-component-definition': 'off',
    'react/button-has-type': 'off',
    'no-const-assign': 'off',
    'react/jsx-filename-extension': 'off',
    'no-restricted-exports': 'off',
    'max-len': 'off',
    'jsx-a11y/label-has-associated-control': 'off',
  },
};
